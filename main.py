from turtle import *

# Function that moves the cursor a given distance and then draws a circle of the given size and color.
def draw_planet(mov_dist, planet_radius, planet_color):
    # Move the cursor without drawing a line
    penup()
    forward(mov_dist)
    pendown()
    # Draw and fill the circle
    begin_fill()
    color(planet_color)
    circle(planet_radius)
    end_fill()

# Set the background color of the canvas and the speed at which the image is drawn
# 0 means the image will be drawn instantaneously, otherwise the speed goes from 1
# to 10, with 1 being the slowest and 10 the fastest.
bgcolor("black")
speed(0)

draw_planet(0,60,"orange")
draw_planet(100,20,"grey")
draw_planet(80,40,"red")
draw_planet(90,30,"green")

# Done command is required by VS Code to not instantly close the turtle popup after drawing the image
done()